package proxy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utils;
import websites.PageBase;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Random;

public class ProxyListPage extends PageBase {

    public ProxyListPage(WebDriver driver) {
        super(driver);
    }

    public void launch(){
        driver.get("https://sslproxies.org/");
    }

    @FindBy(css = "#proxylisttable > tbody")
    private WebElement tblBody;

    public String getRandomProxy(){
        WebElement table = waitForVisible(tblBody);
        List<WebElement> rows = Utils.getChildrenOfElement(table);

        int randomIndex = new Random().nextInt(rows.size());
        WebElement randomRow = rows.get(randomIndex);

        List<WebElement> rowCells = Utils.getChildrenOfElement(randomRow);

        String ip = rowCells.get(0).getText();
        String port = rowCells.get(1).getText();
        return ip + ":" + port;
    }
}
