package proxy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Proxy {

    public static String getProxy() throws Exception {
        int currentAttempts = 0;
        int maxAttempts = 3;
        String proxy = "";

        while (currentAttempts < maxAttempts) {

            try {
                proxy = searchForProxy();
                break;
            } catch (Exception ignored) {
                currentAttempts++;
            }
        }

        if (proxy.equals("")) { throw new Exception("Failed to get proxy"); }

        return proxy;
    }

    private static String searchForProxy(){
        WebDriver driver = null;
        String proxy = "";

        try {
            driver = initChromeDriver();
            ProxyListPage proxyPage = new ProxyListPage(driver);
            proxyPage.launch();
            proxy = proxyPage.getRandomProxy();
            System.out.println("IP: " + proxy);
        } catch (Exception ignored) {}

        finally {
            if (driver != null) {
                driver.quit();
            }
        }

        return proxy;
    }

    private static WebDriver initChromeDriver() {
        String driverPath = "src/main/java/driver/";
        System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        options.addArguments("disable-popup-blocking");
        options.addArguments("--headless");

        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        return driver;
    }

}
