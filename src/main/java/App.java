import db.DBConnection;
import db.dao.DaoImpl;
import driver.DriverSetup;
import org.openqa.selenium.WebDriver;
import proxy.Proxy;
import websites.imobiliare.script.ImobiliareScript;

import java.util.Arrays;

public class App {

    public static void main(String[] args) throws Exception {
        DBConnection.connect(false);
        DaoImpl dao = new DaoImpl();
        WebDriver driver = initDriver();

        if (driver == null) { return; }

        scrapeImobiliare(driver, dao);

        try { driver.quit(); }
        catch (Exception ignored) {}

        DBConnection.tearDown();
    }

    private static void scrapeImobiliare(WebDriver driver, DaoImpl dao) {
        try { ImobiliareScript.scrapeImobiliare(driver, dao);

        } catch (Exception e) {
            System.out.println("Failed to scrape Imobiliare.ro");
            System.out.println("Error message: " + Arrays.toString(e.getStackTrace()));
        }
    }

    private static WebDriver initDriver() {
        WebDriver driver = null;
        String proxy = "";

        try {
            proxy = Proxy.getProxy();
            driver = DriverSetup.launchChromeDriver(proxy);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            DBConnection.tearDown();
        }
        return driver;
    }
}
