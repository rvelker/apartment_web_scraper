package driver;

import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class DriverSetup {

    public static WebDriver launchChromeDriver(String proxy) {
        WebDriver driver = setupChrome(proxy);
        openGoogle(driver);
        return driver;
    }

    private static WebDriver setupChrome(String proxy) {
        String driverPath = "src/main/java/driver/";
        System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        options.addArguments("disable-popup-blocking");

        if (!proxy.equals("")) {
            Proxy px = new Proxy();
            px.setHttpProxy(proxy);
            options.setCapability(CapabilityType.PROXY, px);
        }

        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        return driver;
    }


    private static void openGoogle(WebDriver driver){
        driver.get("https://www.google.com/");
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement btnAcceptCookies = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("#L2AGLb > div"))));
        btnAcceptCookies.click();
    }

    private static String getRandomUserAgent(){
        String chrome = "user-agent=\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36\"";
        String opera = "user-agent=\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 OPR/77.0.4054.203\"";
        String edge = "user-agent=\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.67\"";
        String firefox = "user-agent=\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0\"";
        List<String> userAgentList = List.of(chrome, opera, edge, firefox);
//        return userAgentList.get(new Random().nextInt(userAgentList.size()));
        return opera;
    }

}
