package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static void sleep(long min, long max) {
        long duration = (long) ((Math.random() * (max - min)) + min);

        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void newTab(WebDriver driver) {
        System.out.println("new tab");
        ((JavascriptExecutor)driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
    }

    public static WebElement getParentOfElement(WebElement element, WebDriverWait wait) {
        return waitForVisible(element.findElement(By.xpath("./..")), wait);
    }

    public static List<WebElement> getChildrenOfElement(WebElement element) {
        return new ArrayList<>(element.findElements(By.xpath("./child::*")));
    }

    public static WebElement getNextSibling(WebElement element, WebDriverWait wait) {
        return waitForVisible(element.findElement(By.xpath("following-sibling::*")), wait);
    }


    public static WebElement waitForVisible(WebElement element, WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement waitForClickable(WebElement element, WebDriverWait wait) {
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }


    public static boolean isVisible(WebElement element, WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isClickable(WebElement element, WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(element));
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public static boolean waitForUrlToBe(String url, WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.urlToBe(url));
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    public static boolean waitForUrlToContain(String string, WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.urlContains(string));
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }


    public static void clickElement(WebElement element, WebDriverWait wait) {
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public static void scrollTo(WebElement element, WebDriver driver){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

}
