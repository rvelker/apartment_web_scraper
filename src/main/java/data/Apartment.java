package data;

public class Apartment {

    public static final String CLUJ = "cluj";
    public static final String BAIA_MARE = "baia_mare";

    public Apartment(String city) {
        this.city = city;
    }

    //region properties
    private boolean save = false;

    private String city;

    private String website = "";

    private String url = "";

    private String websiteId = "";

    private String area = "";

    private String story = "";

    private String price = "";

    private int kitchens = 0;

    private int phoneNumber = 0;

    private int rooms = 0;

    private int bathrooms = 0;

    private int balconies = 0;

    private int parkingSpots = 0;

    private String surface = "";
    //endregion

    //region getters / setters
    public String getCity() {
        return city;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website.trim();
    }

    public String getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(String websiteId) {
        this.websiteId = websiteId.trim();
    }

    public int getBalconies() {
        return balconies;
    }

    public void setBalconies(int balconies) {
        this.balconies = balconies;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story.trim();
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area.trim();
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price.trim();
    }

    public void setKitchens(int kitchens) {
        this.kitchens = kitchens;
    }

    public int getKitchens() {
        return kitchens;
    }

    public void setParkingSpots(int parkingSpots) {
        this.parkingSpots = parkingSpots;
    }

    public int getParkingSpots() {
        return parkingSpots;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    //endregion
}
