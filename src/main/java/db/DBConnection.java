package db;

import java.sql.*;

public class DBConnection {

    public static Connection conn = null;

    public static void connect(boolean live) throws Exception {
        String url;

        if (live) {

            url = "jdbc:sqlite:D:\\IntelliJ Projects\\ApartmentWebScraper\\src\\main\\java\\db\\staging\\apartments_live.db";

        } else {

            url = "jdbc:sqlite:D:\\IntelliJ Projects\\ApartmentWebScraper\\src\\main\\java\\db\\staging\\apartments_staging.db";
        }

        conn = DriverManager.getConnection(url);
        System.out.println("Connection to DB successful.");
    }

    public static Connection getConnection() {
        return conn;
    }

    public static void tearDown() {

        try {

            if (conn != null) {
                conn.close();
                System.out.println("Connection to DB closed successfully.");
            }

        } catch (SQLException e) {

            System.out.println("Problem closing DB connection.");
            System.out.println(e.getMessage());

        }
    }

}
