package db.dao;

import data.Apartment;

import java.sql.SQLException;
import java.util.List;

public interface Dao {

     void insert(Apartment apartment) throws SQLException;

     void delete(int id) throws SQLException;

     List<String> getApartmentIds(String city, String website) throws SQLException;

     Apartment getClujByWebsiteId(String id) throws SQLException;

     Apartment getBaiaMareByWebsiteId(String id) throws SQLException;

}
