package db.dao;

import data.Apartment;
import db.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoImpl implements Dao {

    static Connection conn = DBConnection.getConnection();

    @Override
    public void insert(Apartment apartment) {
        String sql = "INSERT INTO " +
                apartment.getCity() +
                "(website, websiteId, area, price, rooms, surface, bathrooms, balconies, story, parking_spots, url) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            prepStatement.setString(1, apartment.getWebsite());
            prepStatement.setString(2, apartment.getWebsiteId());
            prepStatement.setString(3, apartment.getArea());
            prepStatement.setString(4, apartment.getPrice());
            prepStatement.setInt(5, apartment.getRooms());
            prepStatement.setString(6, apartment.getSurface());
            prepStatement.setInt(7, apartment.getBathrooms());
            prepStatement.setInt(8, apartment.getBalconies());
            prepStatement.setString(9, apartment.getStory());
            prepStatement.setInt(10, apartment.getParkingSpots());
            prepStatement.setString(11, apartment.getUrl());
            prepStatement.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public void delete(int id) throws SQLException {

    }

    @Override
    public List<String> getApartmentIds(String city, String website) throws SQLException {
        List<String> ids = new ArrayList<>();
        String query = "SELECT websiteId FROM " + city + " WHERE website = '" + website + "'";

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(query);

        while(rs.next()) {
            String id = rs.getString("websiteId");
            ids.add(id);
        }

        st.close();
        return ids;
    }

    @Override
    public Apartment getClujByWebsiteId(String id) throws SQLException {
        return null;
    }

    @Override
    public Apartment getBaiaMareByWebsiteId(String id) throws SQLException {
        return null;
    }
}
