package websites.imobiliare.pageObjects;

import data.Apartment;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Utils;
import websites.PageBase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ImobiliareSearchPage extends PageBase {

    public ImobiliareSearchPage(WebDriver driver) {
        super(driver);
    }

    public List<Apartment> scrape(String city, List<String> savedApartmentIds) {
        int apartmentsToSCrape = (int) ((Math.random() * (10 - 5)) + 5);
        List<Apartment> apartments = new ArrayList<>();
        setupPage(city);

        while(apartments.size() < apartmentsToSCrape) {
            List<String> apartmentIds = getRandomisedApartmentIds(savedApartmentIds);

            for (String id : apartmentIds) {
                ApartmentPage aptPage = new ApartmentPage(driver, id, city);
                aptPage.openPage();
                aptPage.scrape();
                apartments.add(aptPage.getApartment());
            }

            if (apartments.size() < apartmentsToSCrape) {
                nextPage();
            }
        }
        return apartments;
    }

    //region Web Elements
    @FindBy(id = "titlu_lista")
    private WebElement title;

    @FindBy(id = "grupa_49")
    private WebElement ddlPrice;

    @FindBy(id = "fPretVanzareReferintaD")
    private WebElement ddlMinPrice;

    @FindBy(id = "fPretVanzareReferintaU")
    private WebElement ddlMaxPrice;

    @FindBy(className = "box-anunt")
    private List<WebElement> containersApartments;

    @FindBy(xpath = "//*[@id=\"modalCookies\"]/div/div/div/div[2]/div[2]/div[2]/a")
    private WebElement btnAcceptCookies;

    @FindBy(css = "#container-lista-rezultate > div.paginare > div > a.inainte.butonpaginare")
    private WebElement btnNextPage;
    //endregion

    //region Setup Page methods
    public void setupPage(String city) {
        String url = "";
        if (city.equals(Apartment.CLUJ)) {
            url = "https://www.imobiliare.ro/vanzare-apartamente/cluj-napoca";
        } else if (city.equals(Apartment.BAIA_MARE)) {
            url = "https://www.imobiliare.ro/vanzare-apartamente/baia-mare";
        }
        driver.get(url);

        System.out.println("Opened imobiliare.ro search page");
        acceptCookies();
        setSearchOptions();
        Utils.sleep(18000, 26000);
    }

    private void acceptCookies() {
        click(btnAcceptCookies);
    }

    private void setSearchOptions() {
        setMaximumPrice();
    }

    private void setMaximumPrice() {
        do {
            click(ddlPrice);
        } while (!Utils.isClickable(ddlMaxPrice, driver, 10));

        Utils.sleep(3000, 4500);
        String maxPrice = "250000";
        selectDropdownValue(maxPrice, ddlMaxPrice);
        click(ddlPrice);
    }
    //endregion

    private List<String> getRandomisedApartmentIds(List<String> savedApartmentIds) {
        List<String> ids = getAllApartmentIdsOnPage();
        ids.removeAll(savedApartmentIds);
        if (ids.size() > 15) {
            removeRandomApartmentIds(ids);
        }
        Collections.shuffle(ids);
        return ids;
    }

    private List<WebElement> getApartmentContainerList() {
        return containersApartments;
    }

    private List<String> getAllApartmentIdsOnPage() {
        List<WebElement> containers = getApartmentContainerList();
        List<String> apartmentIds = new ArrayList<>();

        for (WebElement container : containers) {

            String id = container.getAttribute("id");

            if (!id.equals("")) {
                apartmentIds.add(id.replace("anunt-", ""));
            }

        }
        return apartmentIds;
    }

    private static void removeRandomApartmentIds(List<String> apartmentIds) {
        Random rand = new Random();

        int remove = (int) ((Math.random() * (4 - 1)) + 1);

        for (int i = 0; i < remove; i++) {
            int index = rand.nextInt(apartmentIds.size());
            apartmentIds.remove(index);
        }
    }

    public void nextPage() {
        Utils.scrollTo(btnNextPage, driver);
        String scrollUpScript = "window.scrollBy(0,-200)";
        Utils.sleep(500, 1000);


        for (int i = 0; i < 2; i++) {
            ((JavascriptExecutor) driver).executeScript(scrollUpScript);
            Utils.sleep(200, 4000);
        }
        click(btnNextPage);
        Utils.sleep(16000, 24000);
    }
}
