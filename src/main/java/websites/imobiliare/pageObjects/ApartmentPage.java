package websites.imobiliare.pageObjects;

import data.Apartment;
import data.Websites;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Utils;
import websites.PageBase;
import websites.imobiliare.script.ImobiliareScraper;

import java.util.ArrayList;
import java.util.Random;

public class ApartmentPage extends PageBase {

    private String apartmentId;
    private Apartment apartment;
    private JavascriptExecutor jsExecutor;

    public ApartmentPage(WebDriver driver, String apartmentId, String city) {
        super(driver);
        this.apartmentId = apartmentId;
        this.apartment = new Apartment(city);
        apartment.setWebsite(Websites.IMOBILIARE_DOMAIN);
        apartment.setWebsiteId(Websites.IMOBILIARE_ID + apartmentId);
        this.jsExecutor = (JavascriptExecutor) driver;
        System.out.println("opened apartment page: " + apartmentId);
    }

    public String getApartmentId() {
        return apartmentId;
    }

    public Apartment getApartment() {
        return apartment;
    }

    //region WEB ELEMENTS
    @FindBy(css = "#b_detalii_caracteristici > div")
    private WebElement tblCharacteristics;

    @FindBy(css = "#modal-recomandate-exit > div > div > div.modal-header > button")
    private WebElement btnCloseOtherPropertiesPopup;

    @FindBy(css = "#modal-exit-first-time-user-detalii > div > div.modal-header > button")
    private WebElement btnCloseSaveSearchPopup;

    @FindBy(css = "#content-detalii > div.box-titlu.row > div > div > div > div > div.col-lg-9.col-md-9.col-sm-9.col-xs-12")
    private WebElement containerApartmentLocation;

    @FindBy(css = "#box-detalii > div.content.col-lg-8.col-md-8 > section > div.tabel.grafic > div.pret-cerut > span")
    private WebElement containerPrice;

    //endregion

    public void openPage() {
        Utils.newTab(driver);
        String base_url = "https://www.imobiliare.ro/anunt/";
        driver.get(base_url + apartmentId);
        apartment.setUrl(base_url + apartmentId);
        System.out.println("Apt URL: " + driver.getCurrentUrl());
        Utils.sleep(6000, 14000);
    }

    public void scrape() {
        saveApartmentLocation();
        savePrice();
        WebElement tablesContainer = null;

        try { tablesContainer = waitForVisible(tblCharacteristics); }

        catch (Exception ignored) { closePopup(); }

        scrollToImages();
        scrollToCharacteristics();

        if (tablesContainer == null) { tablesContainer = waitForVisible(tblCharacteristics); }

        ImobiliareScraper.scrapeCharacteristics(tablesContainer, apartment);
        Utils.sleep(15000, 30000);
        closePage();
    }

    private void savePrice() {
        String price = waitForVisible(containerPrice).getText();
        apartment.setPrice(price);
    }

    private void saveApartmentLocation() {
        WebElement locationContainer = waitForVisible(containerApartmentLocation);
        String[] location = locationContainer.getText().split(",");
        String area = location[location.length - 1].replace("- Vezi hartă", "");
        apartment.setArea(area);
    }

    private void closePopup() {
        try {

            waitForClickable(btnCloseSaveSearchPopup).click();

        } catch (Exception ignored2) {

            try {

                waitForClickable(btnCloseOtherPropertiesPopup).click();

            } catch (Exception ignored3) { }
        }
    }

    private void scrollToImages() {
        String scrollDownScript = "window.scrollBy(0,200)";
        String scrollUpScript = "window.scrollBy(0,-200)";

        int overshootChance = new Random().nextInt(3);

        for (int i = 0; i < 2; i++) {
            jsExecutor.executeScript(scrollDownScript);
            Utils.sleep(150, 200);
        }

        if (overshootChance == 1) {
            jsExecutor.executeScript(scrollDownScript);
            Utils.sleep(600, 800);
            jsExecutor.executeScript(scrollUpScript);
        }

        if (overshootChance == 2) {
            for (int i = 0; i < 2; i++) {
                jsExecutor.executeScript(scrollDownScript);
                Utils.sleep(150, 200);
            }
            for (int i = 0; i < 2; i++) {
                jsExecutor.executeScript(scrollUpScript);
                Utils.sleep(150, 200);
            }
        }
        Utils.sleep(12000, 24000);
    }

    private void scrollToCharacteristics() {
        String scrollDownScript = "window.scrollBy(0,200)";

        for (int i = 0; i < 4; i++) {
            jsExecutor.executeScript(scrollDownScript);
            Utils.sleep(200, 400);
        }
    }

    public void closePage() {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.close();
        driver.switchTo().window(tabs.get(0));
    }
}
