package websites.imobiliare.script;

import data.Apartment;
import data.Websites;
import db.dao.DaoImpl;
import org.openqa.selenium.*;
import websites.GooglePage;
import websites.imobiliare.pageObjects.ApartmentPage;
import websites.imobiliare.pageObjects.ImobiliareSearchPage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ImobiliareScript {

    private static final String WEBSITE = Websites.IMOBILIARE_DOMAIN;
    private static WebDriver driver;
    private static DaoImpl dao;

    public static void scrapeImobiliare(WebDriver driver, DaoImpl dao) throws SQLException {
        ImobiliareScript.driver = driver;
        ImobiliareScript.dao = dao;

        new GooglePage(driver).searchImobiliareCluj();

        List<Apartment> apartmentsCluj = getApartmentsIn(Apartment.CLUJ);
        addApartmentsToDatabase(apartmentsCluj);

        for (Apartment ap : apartmentsCluj) {
            System.out.println("City: " + ap.getCity());
            System.out.println("Website: " + ap.getWebsite());
            System.out.println("ID: " + ap.getWebsiteId());
            System.out.println("Area: " + ap.getArea());
            System.out.println("Surface: " + ap.getSurface());
            System.out.println("Story: " + ap.getStory());
            System.out.println("Kitchens:" + ap.getKitchens());
            System.out.println("Rooms: " + ap.getRooms());
            System.out.println("Price: " + ap.getPrice());
            System.out.println("Bathrooms: " + ap.getBathrooms());
            System.out.println("Kitchens: " + ap.getKitchens());
            System.out.println("Balconies: " + ap.getBalconies());
            System.out.println("Parking: " + ap.getParkingSpots());
            System.out.println("-----------------------------");
        }
    }

    private static void addApartmentsToDatabase(List<Apartment> clujApartments) {
        for (Apartment apt : clujApartments) {
            dao.insert(apt);
        }
    }

    private static List<Apartment> getApartmentsIn(String city) throws SQLException {
        List<String> savedApartmentIds = new ArrayList<>();

        ImobiliareSearchPage searchPage = new ImobiliareSearchPage(driver);
        return searchPage.scrape(city, savedApartmentIds);
    }

}