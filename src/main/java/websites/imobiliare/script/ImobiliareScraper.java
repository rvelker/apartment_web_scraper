package websites.imobiliare.script;

import data.Apartment;
import org.openqa.selenium.WebElement;
import utils.Utils;

import java.util.List;

public class ImobiliareScraper {

    public static void scrapeCharacteristics(WebElement tableContainer, Apartment apartment) {

        List<WebElement> tableDivisions = Utils.getChildrenOfElement(tableContainer);

        for (WebElement division : tableDivisions) {

            WebElement table = Utils.getChildrenOfElement(division).get(0);
            List<WebElement> rows = Utils.getChildrenOfElement(table);

            try {
                setCharacteristics(rows, apartment);
                apartment.setSave(true);

            } catch (Exception e) {
                apartment.setSave(false);
            }
        }
    }

    private static void setCharacteristics(List<WebElement> rows, Apartment apartment){
        for (WebElement row : rows) {

            String[] characteristics = row.getText().split(":");
            String key = characteristics[0];
            String value = characteristics[1].replace("\n", "");

            switch (key) {

                case "Nr. camere": {
                    apartment.setRooms(Integer.parseInt(value));
                    break;
                }

                case "Suprafaţă utilă":{
                    apartment.setSurface(value);
                    break;
                }

                case "Etaj":{
                    apartment.setStory(value);
                    break;
                }

                case "Nr. băi":{
                    apartment.setBathrooms(Integer.parseInt(value));
                    break;
                }

                case "Nr. balcoane":{
                    apartment.setBalconies(Integer.parseInt(value));
                    break;
                }

                case "Nr. bucătării":{
                    apartment.setKitchens(Integer.parseInt(value));
                    break;
                }

                case "Nr. locuri parcare":{
                    apartment.setParkingSpots(Integer.parseInt(value));
                    break;
                }

            }
        }
    }



}
