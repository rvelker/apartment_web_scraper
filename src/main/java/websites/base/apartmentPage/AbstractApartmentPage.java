package websites.base.apartmentPage;

import data.Apartment;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import websites.base.PageBase;

import java.util.ArrayList;
import java.util.List;

public class AbstractApartmentPage extends PageBase implements InterfaceApartmentPage {

    protected List<String> existingIds;
    protected String apartmentId;
    protected Apartment apartment;
    protected JavascriptExecutor jsExecutor;

    public AbstractApartmentPage(WebDriver driver, String city, List<String> existingIds) {
        super(driver);
        this.apartment = new Apartment(city);
        this.jsExecutor = (JavascriptExecutor) driver;
        this.existingIds = existingIds;
        apartment.setUrl(driver.getCurrentUrl());
    }

    @Override
    public void scrape(WebElement btnViewApartment) {
        try {

            openPage(btnViewApartment);
            if(!apartmentExistsInDatabase()) { saveDetails(); }

        } catch (Exception e) {

            System.out.println("Failed to scrape apartment page");
            e.printStackTrace();

        } finally {

            try {
                closePage();
            } catch (Exception ignored) {}

        }
    }

    @Override
    public boolean apartmentExistsInDatabase() { return false; }

    @Override
    public void openPage(WebElement btnViewApartment) {}

    @Override
    public void saveDetails() { }

    @Override
    public void closePage() {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.close();
        driver.switchTo().window(tabs.get(0));
    }

    @Override
    public Apartment getApartment() { return apartment; }

    @Override
    public String getApartmentId() { return apartmentId; }

    @Override
    public void savePrice() {}

    @Override
    public void saveLocation() {}
}
