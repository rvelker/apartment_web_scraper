package websites.base.apartmentPage;

import data.Apartment;
import org.openqa.selenium.WebElement;

public interface InterfaceApartmentPage {

    void scrape(WebElement btnViewApartment);

    boolean apartmentExistsInDatabase();

    void openPage(WebElement btnViewApartment);

    void saveDetails();

    void closePage();

    void savePrice();

    void saveLocation();

    Apartment getApartment();

    String getApartmentId();
}
