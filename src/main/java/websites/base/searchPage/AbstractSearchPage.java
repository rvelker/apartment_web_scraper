package websites.base.searchPage;

import data.Apartment;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.Utils;
import websites.base.PageBase;

import java.util.List;

public abstract class AbstractSearchPage extends PageBase implements InterfaceSearchPage {

    protected String city;
    protected List<String> existingIds;

    public AbstractSearchPage(WebDriver driver, String city, List<String> existingIds) {
        super(driver);
        this.city = city;
        this.existingIds = existingIds;
    }

    @Override
    public List<Apartment> scrape() {
        setupSearchPage(city);
        Utils.sleep(18000, 24000);
        return cycleThroughApartments(existingIds);
    }

    @Override
    public void setupSearchPage(String city) {
        acceptCookies();
        setMaxPrice();
        setRooms();
    }

    @Override
    public abstract void setMaxPrice();

    @Override
    public abstract void setRooms();

    @Override
    public abstract void acceptCookies();

    @Override
    public abstract List<WebElement> getAllApartmentAdsOnPage();

    @Override
    public abstract List<Apartment> cycleThroughApartments(List<String> existingIds);

    @Override
    public abstract void nextPage();
}


