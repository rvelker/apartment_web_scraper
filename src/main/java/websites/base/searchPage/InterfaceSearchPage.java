package websites.base.searchPage;

import data.Apartment;
import org.openqa.selenium.WebElement;

import java.util.List;

public interface InterfaceSearchPage {

    List<Apartment> scrape();

    void setupSearchPage(String city);

    void setMaxPrice();

    void setRooms();

    void acceptCookies();

    List<WebElement> getAllApartmentAdsOnPage();

    List<Apartment> cycleThroughApartments(List<String> existingApartmentIds);

    void nextPage();



}
