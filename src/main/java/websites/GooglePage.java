package websites;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GooglePage extends PageBase {

    public GooglePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(name = "q")
    private WebElement txtSearch;

    public void searchImobiliareCluj(){
        setText("apartamente imobiliare cluj", txtSearch);
        txtSearch.sendKeys(Keys.ENTER);
        WebElement anchor = waitForClickable(driver.findElement (By.xpath ("//*[contains(text(),'Imobiliare.ro')]")));
        anchor.click();
    }

    public void searchImobiliareBaiaMare(){
        setText("apartamente imobiliare baia mare", txtSearch);
        txtSearch.sendKeys(Keys.ENTER);
        WebElement anchor = waitForClickable(driver.findElement (By.xpath ("//*[contains(text(),'Imobiliare.ro')]")));
        anchor.click();
    }

}
