package websites;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Utils;

public class PageBase {

    public WebDriver driver;
    public WebDriverWait wait;

    public PageBase(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
        PageFactory.initElements(driver, this);
    }

    public WebElement waitForVisible(WebElement element) {
        return Utils.waitForVisible(element, wait);
    }

    public WebElement waitForClickable(WebElement element) {
        return Utils.waitForClickable(element, wait);
    }

    public void click(WebElement element) {
        Utils.clickElement(element, wait);
    }

    public void setText(String value, WebElement element) {
        Utils.waitForClickable(element, wait);
        element.click();
        element.sendKeys(value);
    }

    public void selectDropdownValue(String value, WebElement element) {
        WebElement dropdown = Utils.waitForClickable(element, wait);
        element.click();
        Utils.sleep(2000, 3000);
        Select select = new Select(dropdown);
        select.selectByValue(value);
    }


}
